package com.constants;

public class SecurityConstants {
	public static final String JWT_SECRET = "trungnt_secret_key";
	public static final int JWT_EXPIRATION = 864000;
}
