package com.controllers;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Role;
import com.entity.User;
import com.payloads.request.LoginRequest;
import com.payloads.request.RegisterRequest;
import com.payloads.response.LoginResponse;
import com.payloads.response.RegisterResponse;
import com.services.IRoleService;
import com.services.IUserService;
import com.utils.JwtUtils;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/v1/auth")
@Slf4j
public class AuthController {
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private IUserService userService;
	@Autowired
	private IRoleService roleService;
	@Autowired
	private JwtUtils jwtUtils;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<?> login(@RequestBody LoginRequest request) {
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		// If ok, generate jwt
		String jwt = jwtUtils.generateJwtToken(authentication);

		return ResponseEntity.ok(new LoginResponse(jwt));
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<?> register(@RequestBody RegisterRequest request) {
		if (userService.existsByUsername(request.getUsername()))
			return ResponseEntity.badRequest().body("Error: Username is already taken!");

		User user = new User();
		user.setUsername(request.getUsername());
		user.setPassword(passwordEncoder.encode(request.getPassword()));
		user.setEnable(request.getEnable());

		Set<Role> roles = new HashSet<Role>();
		if (request.getRoles() == null || request.getRoles().length == 0) {
			Role role = roleService.findByName("ROLE_USER");
			log.info("Check ROLE_USER -> {}", role != null ? true : false);
			if (role != null)
				roles.add(role);
			else {
				Role newRole = new Role();
				newRole.setName("ROLE_USER");
				if ((role = roleService.saveOrUpdate(newRole)) != null)
					roles.add(role);
			}
		} else {
			for (String strRole : request.getRoles()) {
				switch (strRole) {
				case "ROLE_ADMIN":
					Role adminRole = roleService.findByName("ROLE_ADMIN");
					log.info("Check ROLE_ADMIN -> {}", adminRole != null ? true : false);
					if (adminRole != null)
						roles.add(adminRole);
					else {
						Role newRole = new Role();
						newRole.setName("ROLE_ADMIN");
						if ((adminRole = roleService.saveOrUpdate(newRole)) != null)
							roles.add(adminRole);
					}
					break;

				default:
					Role userRole = roleService.findByName("ROLE_USER");
					log.info("Check ROLE_USER -> {}", userRole != null ? true : false);
					if (userRole != null)
						roles.add(userRole);
					else {
						Role newRole = new Role();
						newRole.setName("ROLE_USER");
						if ((userRole = roleService.saveOrUpdate(newRole)) != null)
							roles.add(userRole);
					}
					break;
				}
			}
		}
		user.setRoles(roles);
		return ResponseEntity.ok(new RegisterResponse(userService.saveOrUpdate(user)));
	}
}
