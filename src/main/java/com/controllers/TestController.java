package com.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String publicPage() {
		return "Public page";
	}

	@RequestMapping(value = "/user", method = RequestMethod.GET)
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public String userPage() {
		return "User page";
	}

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	@PreAuthorize("hasRole('ADMIN')")
	public String adminPage() {
		return "Admin page";
	}
}
