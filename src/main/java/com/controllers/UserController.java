package com.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.entity.User;
import com.services.IUserService;

import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {
	@Autowired
	private IUserService userService;
	@Autowired
	private PasswordEncoder passwordEncoder;

	@GetMapping("/")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<?> getAll() {
		return ResponseEntity.ok(userService.findAll());
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<?> getOne(@PathVariable int id) {
		return ResponseEntity.ok(userService.findById(id));
	}

	@PostMapping("/")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> create(@RequestBody User user) {
		if (userService.existsByUsername(user.getUsername()))
			return ResponseEntity.badRequest().body("Error: Username is already taken!");
		
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		return ResponseEntity.ok(userService.saveOrUpdate(user));
	}

	@PutMapping("/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> update(@RequestBody User user, @PathVariable int id) {
		if (!userService.existsById(id))
			return ResponseEntity.badRequest().body("Error: Username which have id = " + id + " isn't exists!");
		
		user.setId(id);
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		return ResponseEntity.ok(userService.saveOrUpdate(user));
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> delete(@PathVariable int id) {
		userService.deleteById(id);
		return ResponseEntity.ok("Account which have id is " + id + " have been deleted!");
	}
}
