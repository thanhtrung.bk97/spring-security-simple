package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Role;

@Repository
public interface IRoleRepository extends JpaRepository<Role, Integer> {
	Role findByName(String name);
}
