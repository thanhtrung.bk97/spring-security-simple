package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.User;

@Repository
public interface IUserRepository extends JpaRepository<User, Integer> {
	boolean existsByUsername(String username);

	User findByUsername(String username);
}
