package com.services;

import com.entity.Role;

public interface IRoleService {
	Role findByName(String name);

	Role saveOrUpdate(Role role);
}
