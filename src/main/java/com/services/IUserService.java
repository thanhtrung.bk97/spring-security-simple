package com.services;

import java.util.List;

import com.entity.User;

public interface IUserService {
	boolean existsByUsername(String username);

	User findByUsername(String username);

	User saveOrUpdate(User user);

	void delete(Integer id);

	List<User> findAll();

	User findById(int intValue);

	void deleteById(int intValue);

	boolean existsById(int id);
}
